import { FrontendQueoService } from '../api/frontend-queo.service';
import { NavController, AlertController } from '@ionic/angular';
import { Component } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-comments',
  templateUrl: 'comments.page.html',
  styleUrls: ['comments.page.scss'],
})
export class CommentsPage {

  public dataComments : any;

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public queoService      : FrontendQueoService,
  ) {

    /* Se trae la informacion de los comentarios */
    this.queoService.comments().subscribe( dataComments => {
      if(dataComments['success']==true){
          this.dataComments = dataComments['data'].data;
      }
    },error=>{
      console.log(error);
    });

  }

  ionViewWillEnter(){
    /* Se trae la informacion de los comentarios */
    this.queoService.comments().subscribe( dataComments => {
      if(dataComments['success']==true){
          this.dataComments = dataComments['data'].data;
      }
    },error=>{
      console.log(error);
    });
  }

  /* Metodo para enviar a la vista de createComments */
  public commentsCreate(){
    this.navCtrl.navigateForward('/comments-create');
  }

  /* Metodo para editar el caso */
  public editComment(data, i) {
    this.navCtrl.navigateForward('/comments-edit/'+data.id+'/'+data.postId+'/'+data.userId+'/'+data.user.name+'/'+data.name+'/'+data.email+'/'+data.body+'/'+i);
  }

  /* Metodo para mostrar alerta para eliminar un Comentario */
  async deleteComment(data:any, index) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar el comentario?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'Cancelar',
          cssClass: 'secondary'
        },{
          text: 'Eliminar',
          handler: () => {
            this.queoService.commentDelete(data).subscribe(response => {
              this.dataComments.splice(index, 1);
            },error => {
              console.error( error );
            })
          }
        }
      ]
    });

    await alert.present();
  }

  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
