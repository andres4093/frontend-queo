import { FrontendQueoService } from '../api/frontend-queo.service';
import { NavController, AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.page.html',
  styleUrls: ['./tags.page.scss'],
})
export class TagsPage implements OnInit {

  public dataTags : any;

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public queoService      : FrontendQueoService,
  ) {

    /* Se trae la informacion de las etiquetas */
    this.queoService.tags().subscribe( dataTags => {
      if(dataTags['success']==true){
          this.dataTags = dataTags['data'].data;
      }
    },error=>{
      console.log(error);
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    /* Se trae la informacion de las etiquetas */
    this.queoService.tags().subscribe( dataTags => {
      if(dataTags['success']==true){
          this.dataTags = dataTags['data'].data;
      }
    },error=>{
      console.log(error);
    });
  }

  /* Metodo para enviar a la vista de createComments */
  public tagsCreate(){
    this.navCtrl.navigateForward('/tags-create');
  }

  /* Metodo para editar el caso */
  public editTag(data, i) {
    this.navCtrl.navigateForward('/tags-edit/'+data.id+'/'+data.name+'/'+i);
  }

  /* Metodo para mostrar alerta para eliminar un Comentario */
  async deleteTag(data:any, index) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar la etiqueta?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'Cancelar',
          cssClass: 'secondary'
        },{
          text: 'Eliminar',
          handler: () => {
            this.queoService.tagDelete(data.id).subscribe(response => {
              this.dataTags.splice(index, 1);
            },error => {
              console.log(error);
            });
          }
        }
      ]
    });

    await alert.present();
  }

  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
