import { FrontendQueoService } from '../api/frontend-queo.service';
import { NavController, AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit {

  public dataPosts : any;

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public queoService      : FrontendQueoService,
  ) {

    /* Se trae la informacion de las publicaciones */
    this.queoService.posts().subscribe( dataPosts => {
      if(dataPosts['success']==true){
          this.dataPosts = dataPosts['data'].data;
      }
    },error=>{
      console.log(error);
    });

  }

  ionViewWillEnter(){
    this.queoService.posts().subscribe( dataPosts => {
      if(dataPosts['success']==true){
          this.dataPosts = dataPosts['data'].data;
      }
    },error=>{
      console.log(error);
    });
  }

  ngOnInit() {
  }

  /* Metodo para enviar a la vista de Crear Publicacion */
  public postsCreate(){
    this.navCtrl.navigateForward('/posts-create');
  }

  /* Metodo para editar la Publicacion */
  public editPost(data, i) {
    this.navCtrl.navigateForward('/posts-edit/'+data.id+'/'+data.tagId+'/'+data.user.id+'/'+data.user.name+'/'+data.name+'/'+data.email+'/'+i);
  }

  /* Metodo para mostrar alerta para eliminar una Publicacion */
  async deletePost(data:any, index) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar la publicacion?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'Cancelar',
          cssClass: 'secondary'
        },{
          text: 'Eliminar',
          handler: () => {
            this.queoService.postDelete(data).subscribe(response => {
              this.dataPosts.splice(index, 1);
            },error => {
              console.error( error );
            })
          }
        }
      ]
    });

    await alert.present();
  }

  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }
}
