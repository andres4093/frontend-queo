import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostsEditPage } from './posts-edit.page';

const routes: Routes = [
  {
    path: '',
    component: PostsEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsEditPageRoutingModule {}
