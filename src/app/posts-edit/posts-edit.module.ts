import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostsEditPageRoutingModule } from './posts-edit-routing.module';

import { PostsEditPage } from './posts-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PostsEditPageRoutingModule
  ],
  declarations: [PostsEditPage]
})
export class PostsEditPageModule {}
