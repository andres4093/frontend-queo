import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostsEditPage } from './posts-edit.page';

describe('PostsEditPage', () => {
  let component: PostsEditPage;
  let fixture: ComponentFixture<PostsEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostsEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
