import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-posts-edit',
  templateUrl: './posts-edit.page.html',
  styleUrls: ['./posts-edit.page.scss'],
})
export class PostsEditPage implements OnInit {

  public formPostEdit  : FormGroup;  // Formulario de registro
  public loader        : any;        // Loader
  public index         : any;        // Index
  public id            : any;        // Id del comentario
  public tag           : any;        // Id de la publicacion
  public user          : any;        // Id del usuario
  public userName      : any;        // Nombre del usuario
  public name          : any;        // Nombre
  public email         : any;        // Correo
  public dataTags      : any;        // Etiquetas

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public formBuilder      : FormBuilder,
              public loadingCtrl      : LoadingController,
              public activatedRoute   : ActivatedRoute,
              public queoService      : FrontendQueoService,
  ) {

    /* Se trae la informacion de las etiquetas */
    this.queoService.tags().subscribe( dataTags => {
      if(dataTags['success']==true){
          this.dataTags = dataTags['data'].data;
      }
    },error=>{
      console.log(error);
    });

    /* Validacion de formulario */
    this.formPostEdit   = formBuilder.group({
        etiqueta    : ['', Validators.required],
        usuario     : ['', Validators.required],
        nombre      : ['', Validators.required],
        correo      : ['', Validators.required]
    });

    /* parametros que se traen para mostrar en el fomrulario */
    this.index      = this.activatedRoute.snapshot.paramMap.get('i');
    this.id         = this.activatedRoute.snapshot.paramMap.get('id');
    this.tag        = this.activatedRoute.snapshot.paramMap.get('tagId');
    this.user       = this.activatedRoute.snapshot.paramMap.get('userId');
    this.userName   = this.activatedRoute.snapshot.paramMap.get('userName');
    this.name       = this.activatedRoute.snapshot.paramMap.get('name');
    this.email      = this.activatedRoute.snapshot.paramMap.get('email');
  }

  ngOnInit() {
  }

  /* Metodo de validacion */
  public async onSubmit(){

    this.loader = await this.loadingCtrl.create({
      message : "Actualizando...",
    });
    await this.loader.present();

    let tag, user, name, email;

    /* se valida si tuvo algun cambio los campos */
    if(this.formPostEdit.value.etiqueta!=''){ tag = this.formPostEdit.value.etiqueta; }
    else{ tag = this.tag; }

    if(this.formPostEdit.value.usuario!=''){ user = this.formPostEdit.value.usuario; }
    else{ user = this.user; }

    if(this.formPostEdit.value.nombre!=''){ name = this.formPostEdit.value.nombre; }
    else{ name = this.name; }

    if(this.formPostEdit.value.correo!=''){ email = this.formPostEdit.value.correo }
    else{ email = this.email; }

    /* Cuerpo que se va ha actualizar */
    let dataobj = {
      tag      : tag,
      user     : user,
      name     : name,
      email    : email
    };

    this.editPost(dataobj) // se envia al metodo de editPost
  }

  /* Metodo de autenticacion */
  private async editPost(data){

    /* data que tiene la validacion del formulario */
    const dataobj = {
      id      : this.id, // parametro id que se recibe por el formulario
      tagId   : data.tag,  // parametro etiqueta que se recibe por el formulario
      userId  : data.user,  // parametro usuario que se recibe por el formulario
      name    : data.name,  // parametro nombre que se recibe por el formulario
      email   : data.email,  // parametro correo que se recibe por el formulario
    };

    /* Se edita y se guarda la informacion de las publicaciones */
    this.queoService.postEdit(dataobj).subscribe( dataPostEdit => {
      this.loader.dismiss(); // se detiene el loader
      if(dataPostEdit['success']==true){
        this.navCtrl.navigateRoot('/posts');
      }
    },error=>{
      this.loader.dismiss(); // se detiene el loader
      if(error.status==422){
        const dataobj = {
           header  : 'Verificacion',
           message : 'Alguno de los campos es incorrecto o falta por ingresar',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
      }
    });

  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
