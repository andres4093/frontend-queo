import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TagsCreatePage } from './tags-create.page';

const routes: Routes = [
  {
    path: '',
    component: TagsCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TagsCreatePageRoutingModule {}
