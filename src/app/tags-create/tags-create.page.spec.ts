import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TagsCreatePage } from './tags-create.page';

describe('TagsCreatePage', () => {
  let component: TagsCreatePage;
  let fixture: ComponentFixture<TagsCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagsCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TagsCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
