import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-tags-create',
  templateUrl: './tags-create.page.html',
  styleUrls: ['./tags-create.page.scss'],
})
export class TagsCreatePage implements OnInit {

  public formTag  : FormGroup;  // Formulario de registro
  public loader   : any;        // loader

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public formBuilder      : FormBuilder,
              public loadingCtrl      : LoadingController,
              public queoService      : FrontendQueoService,
  ) {
    /* Validacion de formulario */
    this.formTag   = formBuilder.group({
        nombre        : ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  /* Metodo de validacion */
  public async onSubmit(){

    this.loader = await this.loadingCtrl.create({
      message : "Guardando...",
    });
    await this.loader.present();

    if(this.formTag.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        await this.loader.dismiss(); // se detiene el loader
        this.createTag() // se envia al metodo de editComment
    }
  }

  /* Metodo de autenticacion */
  private async createTag(){

    /* data que tiene la validacion del formulario */
    const dataobj = {
        name      : this.formTag.value.nombre,  // parametro nombre que se recibe por el formulario
    };

    /* se guarda la informacion de las etiquetas */
    this.queoService.tagCreate(dataobj).subscribe( dataTagCreate => {
      if(dataTagCreate['success']==true){
        this.navCtrl.navigateRoot('/tags');
      }
    },error=>{
      if(error.status==422){
        const dataobj = {
           header  : 'Verificacion',
           message : 'Alguno de los campos es incorrecto o falta por ingresar',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
      }
    });

  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
