import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TagsCreatePageRoutingModule } from './tags-create-routing.module';

import { TagsCreatePage } from './tags-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TagsCreatePageRoutingModule
  ],
  declarations: [TagsCreatePage]
})
export class TagsCreatePageModule {}
