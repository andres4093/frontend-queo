import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})

export class FrontendQueoService {

  public apiUrl   = 'http://localhost:8000/api'; // Local
  public headers  : any;
  public token    : any;
  public options  : any;

  constructor(private http        : HttpClient,
              private nativeHttp  : HTTP){

        /* token unico por usuario */
        this.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMzJkZTNmNWZiODdkZjlmNzdiZDI3ZDg1MGQ3NjUwMmU0ZWU5ZDBkOGZlZTNjMzI5YmI2MDQzZWM5NjM0MzNhYTJmM2E2N2NkZDY2OTY5ZGYiLCJpYXQiOjE1ODEzNjkzOTAsIm5iZiI6MTU4MTM2OTM5MCwiZXhwIjoxNjEyOTkxNzkwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.P_YdeTNHf0Zlw0mKPPpJPvxWQWg1jyFqX6lbqiZhXJ7nnc9WB1U1c4orAHHZqZ49wTn99DjJQWOljvBmgIeuKnBiQ5zYx_J-YGYBxHJaoV1BVjXzA1p0nRD-o8uGZPxeZaoxEeNxuBQ_5aGBtU-kpNqgncxgHpc01ZN_IAVK0Ju1ECu9lZo0uNgDqsiCyzE_k1JVD3zR2Zz17f3hSLhW-DfkeRH87tItBT7dnDUj2dVSXaMC7wvQ0ru6_RG-jDrfnpM-2NNXl2tng7WQ2zXlfaAiX2qCJ4Qwac6HWjPQO7tquXH3_ci7czYInHTrlLUlggvsOF371hVRYiH06fJvo7-4cctu8Ma_b27rCyOoY9q371giLQcPeE5vAFHy4XxeKq0du4FLoesaAkDpuuc-1oCoOWU_29Po3nycpK7wP648_dNxM86hzCUGKJQtsdibLBWOg8Y9ssPvm1O6Qg277olGYcODYJeMYAQpi01qBVZj3EiviBvB7pq9-dADDpWwFbOYmRDvgoytoyMgZzFO1Y_OYNOqkyNnYfvq5ePsv_pfkgCZk9zgwBrkglwQ6HpuxPtzA2qHwMKxbewMMaGiDm0m73X1-c1vGWu__c_6vjib6bLWKzPZSna6dBAwOYwNadvAV3ERpkWItMuZGxsaoCUfeFC9hIP6mFg_Km4nSOI";

        /* cabeceras en las peticiones */
        this.options = {
            headers: { Authorization: 'Bearer '+this.token }
        };
  }

  /* Metodo para autenticar usuarios */
  loginUser(data:any) :Observable<any>{
    const dataobj = {
      'username'      : data.email,
      'password'      : data.password,
      'grant_type'    : 'password',
      'client_id'     : '2', // se debe usar siempre el id 2
      'client_secret' : 'Zhcz9zffVcOct8rAvxBG6a1konjqQqcYtwLAWkdA' // el client secret es el que se genera en la base de datos este esta en el id 2
    }

    return this.http.post('http://localhost:8000/oauth/token', dataobj).map(responseLogin => {
    return responseLogin;
    }).catch((error:any) => {
        return Observable.throw(error);
    });
  }

  /* Metodo para registrar usuarios */
  registerUser(data:any) :Observable<any>{
    const dataobj = {
      'email'      : data.email,
      'name'       : data.name,
      'password'   : data.password,
      'c_password' : data.password
    };

    return this.http.post(this.apiUrl + '/register',dataobj).map(responseLogin => {
    return responseLogin;
    }).catch((error:any) => {
        return Observable.throw(error);
    });
  }

  /* -------- Comments ------------ */

  /* Metodo para obtener todos los cometarios */
  comments() {
    return this.http.get(this.apiUrl + '/comments',this.options).map(responseComments => {
    return responseComments;
     }).catch((error:any) => {
         return Observable.throw(error);
     });
  }

  /* Metodo para crear un comentario */
  commentCreate(postData: any): Observable<any> {
    return this.http.post(this.apiUrl+'/comments/store', postData, this.options ).map(responseCommentCreate => {
    return responseCommentCreate;
    },error => {
        console.log(error);
    });
  }

  /* Metodo para editar la informacion de los comentarios */
  commentEdit(data:any) {
    return this.http.put(this.apiUrl + '/comments/update/'+data.id, data, this.options).map(respondeCommentEdit => {
    return respondeCommentEdit;
    },error => {
      console.log(error);
    });
  }

  /* Metodo para eliminar un comentario*/
  commentDelete(data: any) {
    return this.http.delete(this.apiUrl + '/comments/destroy/'+data.id, this.options);
  }

  /* ----------- Tags ---------- */

  /* Metodo para obtener todos las etiquetas */
  tags() {
    return this.http.get(this.apiUrl + '/tags', this.options).map(responseTags => {
    return responseTags;
     }).catch((error:any) => {
         return Observable.throw(error);
     });
  }

  /* Metodo para crear una etiqueta */
  tagCreate(postData: any): Observable<any> {
    return this.http.post(this.apiUrl+'/tags/store', postData, this.options ).map(responseTagsCreate => {
    return responseTagsCreate;
      },error => {
        console.log(error);
    });
  }

  /* Metodo para editar la informacion de las etiquetas */
  tagEdit(data:any) {
    return this.http.put(this.apiUrl + '/tags/update/'+data.id, data, this.options).map(respondeTagEdit => {
    return respondeTagEdit;
    },error => {
      console.log(error);
    });
  }

  /* Metodo para eliminar un comentario*/
  tagDelete(data:any) {
    return this.http.delete(this.apiUrl + '/tags/destroy/'+data, this.options);
  }

  /* ----------- Posts ---------- */
  /* Metodo apra obtener todas las publicaciones */
  posts() {
    return this.http.get(this.apiUrl + '/posts',this.options).map(responsePosts => {
    return responsePosts;
     }).catch((error:any) => {
         return Observable.throw(error);
     });
  }

  /* Metodo para crear una publicacion */
  postCreate(postData: any): Observable<any> {
    return this.http.post(this.apiUrl+'/posts/store', postData, this.options ).map(responsePostCreate => {
    return responsePostCreate;
      },error => {
        console.log(error);
    });
  }

  /* Metodo para editar la informacion de las publicaciones*/
  postEdit(data:any) {
    return this.http.put(this.apiUrl + '/posts/update/'+data.id, data, this.options).map(respondePostEdit => {
    return respondePostEdit;
    },error => {
      console.log(error);
    });
  }

  /* Metodo para eliminar un comentario*/
  postDelete(data:any) {
    return this.http.delete(this.apiUrl + '/posts/destroy/'+data.id, this.options);
  }

}
