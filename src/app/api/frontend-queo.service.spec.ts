import { TestBed } from '@angular/core/testing';

import { FrontendQueoService } from './frontend-queo.service';

describe('FrontendQueoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FrontendQueoService = TestBed.get(FrontendQueoService);
    expect(service).toBeTruthy();
  });
});
