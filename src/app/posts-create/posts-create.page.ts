import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-posts-create',
  templateUrl: './posts-create.page.html',
  styleUrls: ['./posts-create.page.scss'],
})
export class PostsCreatePage implements OnInit {

  public formPost   : FormGroup;  // Formulario de registro
  public loader     : any;        // loader
  public dataTags   : any;        // Etiquetas

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public formBuilder      : FormBuilder,
              public loadingCtrl      : LoadingController,
              public queoService      : FrontendQueoService,
  ) {

    /* Se trae la informacion de las etiquetas */
    this.queoService.tags().subscribe( dataTags => {
      if(dataTags['success']==true){
          this.dataTags = dataTags['data'].data;
      }
    },error=>{
      console.log(error);
    });

    /* Validacion de formulario */
    this.formPost   = formBuilder.group({
        etiqueta    : ['', Validators.required],
        usuario     : ['', Validators.required],
        nombre      : ['', Validators.required],
        correo      : ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  /* Metodo de validacion */
  public async onSubmit(){
    this.loader = await this.loadingCtrl.create({
      message : "Guardando...",
    });
    await this.loader.present();

    if(this.formPost.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        await this.loader.dismiss(); // se detiene el loader
        this.createPost() // se envia al metodo de createPost
    }
  }

  /* Metodo de autenticacion */
  private async createPost(){

    /* data que tiene la validacion del formulario */
    const dataobj = {
        tagId    : this.formPost.value.etiqueta,  // parametro etiqueta que se recibe por el formulario
        userId   : this.formPost.value.usuario,  // parametro usuario que se recibe por el formulario
        name     : this.formPost.value.nombre,  // parametro nombre que se recibe por el formulario
        email    : this.formPost.value.correo,  // parametro correo que se recibe por el formulario
    };

    /* Se guarda la informacion de las publicaciones */
    this.queoService.postCreate(dataobj).subscribe( dataPostCreate => {
      if(dataPostCreate['success']==true){
        this.navCtrl.navigateRoot('/posts');
      }
    },error=>{
      if(error.status==422){
        const dataobj = {
           header  : 'Verificacion',
           message : 'Alguno de los campos es incorrecto o falta por ingresar',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
      }
    });

  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }


  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
