import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostsCreatePage } from './posts-create.page';

describe('PostsCreatePage', () => {
  let component: PostsCreatePage;
  let fixture: ComponentFixture<PostsCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostsCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
