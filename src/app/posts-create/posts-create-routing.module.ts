import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostsCreatePage } from './posts-create.page';

const routes: Routes = [
  {
    path: '',
    component: PostsCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsCreatePageRoutingModule {}
