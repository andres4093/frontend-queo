import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostsCreatePageRoutingModule } from './posts-create-routing.module';

import { PostsCreatePage } from './posts-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PostsCreatePageRoutingModule
  ],
  declarations: [PostsCreatePage]
})
export class PostsCreatePageModule {}
