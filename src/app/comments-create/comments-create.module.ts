import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommentsCreatePageRoutingModule } from './comments-create-routing.module';

import { CommentsCreatePage } from './comments-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CommentsCreatePageRoutingModule
  ],
  declarations: [CommentsCreatePage]
})
export class CommentsCreatePageModule {}
