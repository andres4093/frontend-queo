import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-comments-create',
  templateUrl: './comments-create.page.html',
  styleUrls: ['./comments-create.page.scss'],
})
export class CommentsCreatePage implements OnInit {

  public formComment  : FormGroup;  // Formulario de registro
  public loader       : any;        // loader
  public dataPosts    : any;

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public formBuilder      : FormBuilder,
              public loadingCtrl      : LoadingController,
              public queoService      : FrontendQueoService,
  ) {

    /* Se trae la informacion de las publicaciones */
    this.queoService.posts().subscribe( dataPosts => {
      if(dataPosts['success']==true){
          this.dataPosts = dataPosts['data'].data;
      }
    },error=>{
      console.log(error);
    });

    /* Validacion de formulario */
    this.formComment   = formBuilder.group({
        publicacion  : ['', Validators.required],
        usuario      : ['', Validators.required],
        nombre       : ['', Validators.required],
        correo       : ['', Validators.required],
        mensaje      : ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  /* Metodo de validacion */
  public async onSubmit(){
    this.loader = await this.loadingCtrl.create({
      message : "Guardando...",
    });
    await this.loader.present();

    if(this.formComment.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        await this.loader.dismiss(); // se detiene el loader
        this.createComment() // se envia al metodo de createComment
    }
  }

  /* Metodo de autenticacion */
  private async createComment(){

    /* data que tiene la validacion del formulario */
    const dataobj = {
        postId  : this.formComment.value.publicacion,  // parametro publicacion que se recibe por el formulario
        userId  : this.formComment.value.usuario,  // parametro usuario que se recibe por el formulario
        name    : this.formComment.value.nombre,  // parametro nombre que se recibe por el formulario
        email   : this.formComment.value.correo,  // parametro correo que se recibe por el formulario
        body    : this.formComment.value.mensaje,  // parametro mensaje que se recibe por el formulario
    };

    /* Se guarda la informacion de los comentarios */
    this.queoService.commentCreate(dataobj).subscribe( dataCommentCreate => {
      if(dataCommentCreate['success']==true){
        this.navCtrl.navigateRoot('/comments');
      }
    },error=>{
      if(error.status==422){
        const dataobj = {
           header  : 'Verificacion',
           message : 'Alguno de los campos es incorrecto o falta por ingresar',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
      }
    });

  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }


  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
