import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommentsCreatePage } from './comments-create.page';

describe('CommentsCreatePage', () => {
  let component: CommentsCreatePage;
  let fixture: ComponentFixture<CommentsCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommentsCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
