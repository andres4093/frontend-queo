import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public Ocultar       : any = true;
  public Mostrar       : any = true;
  public formRegister  : FormGroup;  // Formulario de registro
  public loader        : any;        // loader
  public passwordType  : String = 'password';

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public formBuilder      : FormBuilder,
              public loadingCtrl      : LoadingController,
              public queoService      : FrontendQueoService,
  ) {

    this.Ocultar = false;
    /* Validacion de formulario */
    this.formRegister   = formBuilder.group({
        email       : ['', Validators.required],
        name        : ['', Validators.required],
        password    : ['', Validators.required]
    });
  }


  ngOnInit() {}

  /* Metodo de validacion */
  public async onSubmit(){
    this.loader = await this.loadingCtrl.create({
      message : "Ingresando...",
    });
    await this.loader.present();

    if(this.formRegister.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        await this.loader.dismiss(); // se detiene el loader
        this.auth() // se envia al metodo de auth
    }
  }

  /* Metodo de autenticacion */
  private async auth(){

    /* data que tiene la validacion del usuario */
    const dataobj = {
        email      : this.formRegister.value.email,     // parametro email que se recibe por el formulario de registro
        name       : this.formRegister.value.name,      // parametro name que se recibe por el formulario de registro
        password   : this.formRegister.value.password,  // parametro password que se recibe por el formulario de registro
    };

    /* se crea el usuario en la base de datos */
    this.queoService.registerUser(dataobj).subscribe( dataRegisterUser => {
      if(dataRegisterUser['data'].token!=''){
        this.navCtrl.navigateRoot('/comments'); // luego de validar autenticacion se envia al home
      }
   },error=>{
       const dataobj = {
         header  : 'Autenticación',
         message : 'Usuario ó contraseña incorrectos',
       }
       this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
   });
  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

  /* Metodos para ir al login */
  public login(){
    this.navCtrl.navigateRoot('/login');
  }

}
