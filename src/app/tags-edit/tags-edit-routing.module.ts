import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TagsEditPage } from './tags-edit.page';

const routes: Routes = [
  {
    path: '',
    component: TagsEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TagsEditPageRoutingModule {}
