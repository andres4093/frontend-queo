import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TagsEditPageRoutingModule } from './tags-edit-routing.module';

import { TagsEditPage } from './tags-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TagsEditPageRoutingModule
  ],
  declarations: [TagsEditPage]
})
export class TagsEditPageModule {}
