import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-tags-edit',
  templateUrl: './tags-edit.page.html',
  styleUrls: ['./tags-edit.page.scss'],
})
export class TagsEditPage implements OnInit {

  public formTagEdit  : FormGroup;  // Formulario de registro
  public loader       : any;        // loader
  public index        : any;        // index
  public id           : any;        // id del comentario
  public name         : any;        // nombre

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public formBuilder      : FormBuilder,
              public loadingCtrl      : LoadingController,
              public activatedRoute   : ActivatedRoute,
              public queoService      : FrontendQueoService,
  ) {
    /* Validacion de formulario */
    this.formTagEdit   = formBuilder.group({
        nombre        : ['', Validators.required]
    });

    /* parametros que se traen para mostrar en el fomrulario */
    this.index      = this.activatedRoute.snapshot.paramMap.get('i');
    this.id         = this.activatedRoute.snapshot.paramMap.get('id');
    this.name       = this.activatedRoute.snapshot.paramMap.get('name');

  }

  ngOnInit() {
  }

  /* Metodo de validacion */
  public async onSubmit(){
    this.loader = await this.loadingCtrl.create({
      message : "Actualizando...",
    });
    await this.loader.present();

    let name;

    /* se valida si tuvo algun cambio los campos */
    if(this.formTagEdit.value.name!=''){ name = this.formTagEdit.value.name; }
    else{ name = this.name; }

    let dataobj = {
      name: name
    }

    this.editTag(dataobj) // se envia al metodo de editTag

  }

  /* Metodo de autenticacion */
  private async editTag(data){

    /* data que tiene la validacion del formulario */
    const dataobj = {
        id      : this.id, // parametro id que se recibe por el formulario
        name    : data.name,  // parametro nombre que se recibe por el formulario
    };

    /* Se edita y se guarda la informacion de las publicaciones */
    this.queoService.tagEdit(dataobj).subscribe( dataTagEdit => {
      this.loader.dismiss(); // se detiene el loader
      if(dataTagEdit['success']==true){
        this.navCtrl.navigateRoot('/tags');
      }
    },error=>{
      this.loader.dismiss(); // se detiene el loader
      if(error.status==422){
        const dataobj = {
           header  : 'Verificacion',
           message : 'Alguno de los campos es incorrecto o falta por ingresar',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
      }
    });

  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
