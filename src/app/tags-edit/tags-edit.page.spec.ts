import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TagsEditPage } from './tags-edit.page';

describe('TagsEditPage', () => {
  let component: TagsEditPage;
  let fixture: ComponentFixture<TagsEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagsEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TagsEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
