import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public Ocultar    : any = true;
  public Mostrar    : any = true;
  public formLogin  : FormGroup;  // Formulario de registro
  public loader     : any;        // loader

  passwordType: String = 'password';

    constructor(public navCtrl          : NavController,
                public alertController  : AlertController,
                public formBuilder      : FormBuilder,
                public loadingCtrl      : LoadingController,
                public queoService      : FrontendQueoService,
    ) {

    this.Ocultar = false;
    /* Validacion de formulario */
    this.formLogin   = formBuilder.group({
        email        : ['', Validators.required],
        password     : ['', Validators.required]
    });
  }


  ngOnInit() {}

  /* Metodo de validacion */
  public async onSubmit(){
    this.loader = await this.loadingCtrl.create({
      message : "Ingresando...",
    });
    await this.loader.present();

    if(this.formLogin.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        await this.loader.dismiss(); // se detiene el loader
        this.auth() // se envia al metodo de auth
    }
  }

  /* Metodo de autenticacion */
  private async auth(){

    /* data que tiene la validacion del usuario */
    const dataobj = {
        email      : this.formLogin.value.email,     // parametro email que se recibe por el formulario de ingreso
        password   : this.formLogin.value.password,  // parametro password que se recibe por el formulario de ingreso
    };

    /* se verifica en la base de datos la existe del usuario */
    this.queoService.loginUser(dataobj).subscribe( dataUser => {
      if(dataUser['access_token']!=''){
        this.navCtrl.navigateRoot('/comments'); // luego de validar autenticacion se envia al home
      }
   },error=>{
       const dataobj = {
         header  : 'Autenticación',
         message : 'Usuario ó contraseña incorrectos',
       }
       this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
   });
  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

  /* Metodo para mostrar o ocultar contraseña */
  public cambio(value){
    if(value==0){
      this.Ocultar = true;
      this.Mostrar = false;
      this.passwordType = 'text';
    }else if(value==1){
      this.Mostrar = true;
      this.Ocultar = false;
      this.passwordType = 'password';

      // this.formLogin.type.password = 'password';
    }
  }

  /* Metodos para ir al registro */
  public register(){
    this.navCtrl.navigateRoot('/register');
  }
}
