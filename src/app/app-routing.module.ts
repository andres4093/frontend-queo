import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  /* Comentarios */
  { path: 'comments', loadChildren: () => import('./comments/comments.module').then( m => m.CommentsPageModule)},
  { path: 'comments-create', loadChildren: () => import('./comments-create/comments-create.module').then( m => m.CommentsCreatePageModule)},
  { path: 'comments-edit/:id/:postId/:userId/:userName/:name/:email/:body/:i', loadChildren: () => import('./comments-edit/comments-edit.module').then( m => m.CommentsEditPageModule)},
  /* Etiquetas */
  { path: 'tags', loadChildren: () => import('./tags/tags.module').then( m => m.TagsPageModule)},
  { path: 'tags-create', loadChildren: () => import('./tags-create/tags-create.module').then( m => m.TagsCreatePageModule)},
  { path: 'tags-edit/:id/:name/:i', loadChildren: () => import('./tags-edit/tags-edit.module').then( m => m.TagsEditPageModule)},
  /* Publicaciones */
  { path: 'posts', loadChildren: () => import('./posts/posts.module').then( m => m.PostsPageModule)},
  { path: 'posts-create', loadChildren: () => import('./posts-create/posts-create.module').then( m => m.PostsCreatePageModule)},
  { path: 'posts-edit/:id/:tagsId/:userId/:userName/:name/:email/:i', loadChildren: () => import('./posts-edit/posts-edit.module').then( m => m.PostsEditPageModule)},
  /* Ingreso al sistema */
  { path: 'login', loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)},
  /* Registro en el sistema */
  { path: 'register', loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
