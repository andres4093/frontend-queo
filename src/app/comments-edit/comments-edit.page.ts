import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrontendQueoService } from '../api/frontend-queo.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, from } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-comments-edit',
  templateUrl: './comments-edit.page.html',
  styleUrls: ['./comments-edit.page.scss'],
})
export class CommentsEditPage implements OnInit {

  public formCommentEdit  : FormGroup;  // Formulario de registro
  public loader           : any;        // loader
  public index            : any;        // index
  public id               : any;        // id del comentario
  public postId           : any;        // id de la publicacion
  public userId           : any;        // id del usuario
  public userName         : any;        // nombre del usuario
  public name             : any;        // nombre
  public email            : any;        // correo
  public body             : any;        // cuerpo del mensaje
  public dataPosts        : any;

  constructor(public navCtrl          : NavController,
              public alertController  : AlertController,
              public formBuilder      : FormBuilder,
              public loadingCtrl      : LoadingController,
              public activatedRoute   : ActivatedRoute,
              public queoService      : FrontendQueoService,
  ) {

    /* Se trae la informacion de las publicaciones */
    this.queoService.posts().subscribe( dataPosts => {
      if(dataPosts['success']==true){
          this.dataPosts = dataPosts['data'].data;
      }
    },error=>{
      console.log(error);
    });

    /* Validacion de formulario */
    this.formCommentEdit  = formBuilder.group({
        publicacion  : ['', Validators.required],
        usuario      : ['', Validators.required],
        nombre       : ['', Validators.required],
        correo       : ['', Validators.required],
        mensaje      : ['', Validators.required]
    });

    /* parametros que se traen para mostrar en el fomrulario */
    this.index      = this.activatedRoute.snapshot.paramMap.get('i');
    this.id         = this.activatedRoute.snapshot.paramMap.get('id');
    this.postId     = this.activatedRoute.snapshot.paramMap.get('postId');
    this.userId     = this.activatedRoute.snapshot.paramMap.get('userId');
    this.userName   = this.activatedRoute.snapshot.paramMap.get('userName');
    this.name       = this.activatedRoute.snapshot.paramMap.get('name');
    this.email      = this.activatedRoute.snapshot.paramMap.get('email');
    this.body       = this.activatedRoute.snapshot.paramMap.get('body');
  }

  ngOnInit() {
  }

  /* Metodo de validacion */
  public async onSubmit(){
    this.loader = await this.loadingCtrl.create({
      message : "Actualizando...",
    });
    await this.loader.present();

    let post, user, name, email, body;

    /* se valida si tuvo algun cambio los campos */
    if(this.formCommentEdit.value.publicacion!=''){ post = this.formCommentEdit.value.publicacion; }
    else{ post = this.postId; }

    if(this.formCommentEdit.value.usuario!=''){ user = this.formCommentEdit.value.usuario; }
    else{ user = this.userId; }

    if(this.formCommentEdit.value.nombre!=''){ name = this.formCommentEdit.value.nombre; }
    else{ name = this.name; }

    if(this.formCommentEdit.value.correo!=''){ email = this.formCommentEdit.value.correo }
    else{ email = this.email; }

    if(this.formCommentEdit.value.mensaje!=''){ body = this.formCommentEdit.value.mensaje }
    else{ body = this.body; }

    /* Cuerpo que se va ha actualizar */
    let dataobj = {
      publicacion   : post,
      usuario       : user,
      nombre        : name,
      correo        : email,
      mensaje       : body
    };

    this.editComment(dataobj) // se envia al metodo de editComment
  }

  /* Metodo de autenticacion */
  private async editComment(data){

    /* data que tiene la validacion del formulario */
    const dataobj = {
        id       : this.id, // parametro id que se recibe por el formulario
        postId   : data.publicacion,  // parametro publicacion que se recibe por el formulario
        userId   : data.usuario,  // parametro usuario que se recibe por el formulario
        name     : data.nombre,  // parametro nombre que se recibe por el formulario
        email    : data.correo,  // parametro correo que se recibe por el formulario
        body     : data.mensaje,  // parametro mensaje que se recibe por el formulario
    };

    /* Se edita y se guarda la informacion de los comentarios */
    this.queoService.commentEdit(dataobj).subscribe( dataCommentEdit => {
      this.loader.dismiss(); // se detiene el loader
      if(dataCommentEdit['success']==true){
        this.navCtrl.navigateRoot('/comments');
      }
    },error=>{
      this.loader.dismiss(); // se detiene el loader
      if(error.status==422){
        const dataobj = {
           header  : 'Verificacion',
           message : 'Alguno de los campos es incorrecto o falta por ingresar',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
      }
    });

  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

  /* Metodo para finalizar la sesion de un usuario y enviarlo a la vista de login */
  public exit(){
    this.navCtrl.navigateRoot('/login');
  }

}
