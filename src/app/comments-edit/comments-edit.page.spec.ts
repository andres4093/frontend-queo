import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommentsEditPage } from './comments-edit.page';

describe('CommentsEditPage', () => {
  let component: CommentsEditPage;
  let fixture: ComponentFixture<CommentsEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommentsEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
