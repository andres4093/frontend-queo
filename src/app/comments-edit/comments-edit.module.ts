import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommentsEditPageRoutingModule } from './comments-edit-routing.module';

import { CommentsEditPage } from './comments-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CommentsEditPageRoutingModule
  ],
  declarations: [CommentsEditPage]
})
export class CommentsEditPageModule {}
