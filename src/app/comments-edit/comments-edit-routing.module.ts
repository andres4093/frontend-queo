import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommentsEditPage } from './comments-edit.page';

const routes: Routes = [
  {
    path: '',
    component: CommentsEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommentsEditPageRoutingModule {}
